# scale-logo



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description | Type                   | Default     |
| ---------- | ---------- | ----------- | ---------------------- | ----------- |
| `href`     | `href`     |             | `string`               | `''`        |
| `language` | `language` |             | `"de" \| "en"`         | `'de'`      |
| `size`     | `size`     |             | `number`               | `36`        |
| `variant`  | `variant`  |             | `"magenta" \| "white"` | `'magenta'` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
