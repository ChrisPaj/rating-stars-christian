import { Component, Prop, h } from '@stencil/core';

const languages = {
  de: 'Erleben was verbindet',
  en: 'Life is for sharing',
}

const variants = {
  white: '#fff',
  magenta: '#e20074',
}

@Component({
  tag: 'scale-logo',
  styleUrl: 'scale-logo.css',
  shadow: true,
})
export class Logo {
  @Prop() language: 'de'|'en' = 'de'
  @Prop() variant: 'white' | 'magenta' = 'magenta'
  @Prop() size: number = 36
  @Prop() href: string = ''

  markup() {
    return [
      <svg class="logo__image" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 74 36">
        <path fill={variants[this.variant]} fill-rule="nonzero" d="M29.15968 0l.36407 12.7002-1.9135.33021c-.3556-3.74233-1.33775-6.51944-2.94645-8.33133-1.69336-1.89657-4.0302-2.88718-7.02745-2.97185v26.5773c0 2.3199.33021 3.827 1.00755 4.52127.57575.59268 1.54943.95675 2.92105 1.09222.4064.03387 1.10069.0508 2.08284.0508v2.03203H5.87596v-2.03203c.98215 0 1.67643-.01693 2.08284-.0508 1.37162-.13547 2.3453-.49954 2.92104-1.09222.67735-.69428.99909-2.20137.99909-4.52127V1.72723C8.88168 1.8119 6.5533 2.8025 4.85995 4.69908c-1.6087 1.8119-2.59085 4.589-2.94645 8.33133L0 12.70021.36407 0h28.7956zm.36407 16.56107v7.28992h-7.28992v-7.28992h7.28992zm21.86975 0v7.28992H44.1036v-7.28992h7.28991zm21.86976 0v7.28992h-7.28992v-7.28992h7.28992zm-66.05489-.18526v7.20838H0V16.3758h7.20837z"/>
      </svg>,
      <span class="logo__claim" style={{color: variants[this.variant]}}>
        {languages[this.language]}
      </span>
    ]
  }

  render() {
    const Tag = this.href !== '' ? 'a' : 'div';

    const hrefCondition = this.href ? {
      href: this.href,
    } : {}

    // href = '' 
    // Tag = div 
    // hrefCondition = {}
    // <div class="logo" style="--logo-size: 36px;"></div>
   
    // href = 'http://telekom.de' 
    // Tag = a 
    // hrefCondition = {href: 'http://telekom.de', target: 'xxx'}
    // <a class="logo" style="--logo-size: 36px;" href="http://telekom.de" target="xxx"></a>

    return (
      <Tag 
        class="logo" 
        style={{
          '--logo-size': `${this.size}px`
        }} 
        {...hrefCondition}
      >
        {this.markup()}
      </Tag>
    )
  }
}
