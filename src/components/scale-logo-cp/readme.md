# scale-logo-cp



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description | Type                   | Default     |
| ---------- | ---------- | ----------- | ---------------------- | ----------- |
| `href`     | `href`     |             | `string`               | `''`        |
| `language` | `language` |             | `"de" \| "en"`         | `'de'`      |
| `size`     | `size`     |             | `number`               | `54`        |
| `target`   | `target`   |             | `"_blank" \| "_self"`  | `'_blank'`  |
| `variant`  | `variant`  |             | `"magenta" \| "white"` | `'magenta'` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
